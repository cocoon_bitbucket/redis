package api

import (

	"flag"
	"net/http"
	"log"
	"github.com/garyburd/redigo/redis"
	"time"
	"os"
	"os/exec"
	"strings"
	"github.com/prometheus/client_golang/prometheus/promhttp"

)


var (

	RedisUrl string
	Pool redis.Pool

	redisAddr        = flag.String("redis.addr", getEnv("REDIS_ADDR", ""), "Address of one or more redis nodes, separated by separator")
	redisFile        = flag.String("redis.file", getEnv("REDIS_FILE", ""), "Path to file containing one or more redis nodes, separated by newline. NOTE: mutually exclusive with redis.addr")
	redisPassword    = flag.String("redis.password", getEnv("REDIS_PASSWORD", ""), "Password for one or more redis nodes, separated by separator")
	namespace        = flag.String("namespace", "redis", "Namespace for metrics")
	checkKeys        = flag.String("check-keys", "", "Comma separated list of keys to export value and length/size")
	listenAddress    = flag.String("web.listen-address", ":9121", "Address to listen on for web interface and telemetry.")
	metricPath       = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
	separator        = flag.String("separator", ",", "separator used to split redis.addr, redis.password and redis.alias into several elements.")
	redisAlias       = flag.String("redis.alias", getEnv("REDIS_ALIAS", ""), "Redis instance alias for one or more redis nodes, separated by separator")

	redisExecutable  =  flag.String("redis-server", "redis-server", "Name od redis-server")

)


func setup() {

	if * redisAddr == "" {
		* redisAddr = "127.0.0.1:6379"
	}
	if strings.Contains(*redisAddr,","){
		// multiple addrs
		nodes := strings.Split(*redisAddr,",")
		if len(nodes) > 0 {
			RedisUrl =  "redis://" +  nodes[0]  + "/0"
		} else {
			RedisUrl = "redis://" + *redisAddr + "/0"
		}
	} else {
		RedisUrl = "redis://" + *redisAddr + "/0"
	}

	// set redis Pool
	Pool = GetRedisPool(RedisUrl)

	// set redis-server executable
	var err error
	*redisExecutable,err = exec.LookPath(*redisExecutable)
	if err != nil {
		log.Fatal("Cannot find redis server executable : " + *redisExecutable)
	}

	// check if server is running
	cnx := Pool.Get()
	defer cnx.Close()
	// let some time for a redis server to come up
	time.Sleep( 1 * time.Second)
	_,err = cnx.Do("PING")
	if err != nil {
		log.Fatal("No redis-server at " + RedisUrl)
		// no redis-server : start it
		//log.Printf("No redis server: try to launch it\n")
		//cmd := exec.Command(*redisExecutable)
		//err := cmd.Start()
		//  	if err != nil {
		//	  		log.Fatal(err)
		//	  	}
		//log.Printf("redis server launched\n")
	}

	return
}



func Server() {

	flag.Parse()

	setup()

	InitMetrics()

	// handle ping
	http.HandleFunc("/ping",func(w http.ResponseWriter, r *http.Request) {
		HandleSimpleCommand(w,r,"PING")
	})

	// handle save
	http.HandleFunc("/save",func(w http.ResponseWriter, r *http.Request) {
		HandleSimpleCommand(w,r,"SAVE")
	})

	// handle INFO
	http.HandleFunc("/info",func(w http.ResponseWriter, r *http.Request) {
		HandleSimpleCommand(w,r,"INFO")
	})

	// handle INFO
	http.HandleFunc("/time",func(w http.ResponseWriter, r *http.Request) {
		HandleSimpleCommand(w,r,"TIME")
	})

	// handle COMMAND
	http.HandleFunc("/command",func(w http.ResponseWriter, r *http.Request) {
		HandleSimpleCommand(w,r,"COMMAND")
	})


	// handle MULTI
	http.HandleFunc("/multi",func(w http.ResponseWriter, r *http.Request) {
		HandleSimpleCommand(w,r,"MULTI")
	})

	// handle INFO
	http.HandleFunc("/exec",func(w http.ResponseWriter, r *http.Request) {
		HandleSimpleCommand(w,r,"EXEC")
	})



	// handle raw api
	http.HandleFunc("/raw/" , RawHandler )

	//  handle /metrics with prometheus metrics
	http.Handle(*metricPath,promhttp.Handler())

	//log.Printf("running http redis metrics server at: " + *listenAddress + " for redis server: " + RedisUrl)
	//log.Fatal(http.ListenAndServe(*listenAddress, nil))

}


func StartHttpServer() *http.Server {
	//
	//

	// Setup server
	Server()

	//srv := &http.Server{Addr: ":8080"}
	srv := &http.Server{Addr: * listenAddress}

	go func() {
		log.Printf("running http redis metrics server at: " + *listenAddress + " for redis server: " + RedisUrl)
		if err := srv.ListenAndServe(); err != nil {
			// cannot panic, because this probably is an intentional close
			log.Printf("Httpserver: ListenAndServe() error: %s", err)
		}
	}()

	// returning reference so caller can call Shutdown()
	return srv
}




func GetRedisPool( redisUrl string) redis.Pool {

	if redisUrl == "" {
		redisUrl = "redis://127.0.0.1:6379/0"
	}

	pool :=  redis.Pool{
		MaxIdle: 3,
		//MaxActive: 50,
		IdleTimeout: 240 * time.Second,
		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return pool
}


// getEnv gets an environment variable from a given key and if it doesn't exist,
// returns defaultVal given.
func getEnv(key string, defaultVal string) string {
	if envVal, ok := os.LookupEnv(key); ok {
		return envVal
	}
	return defaultVal
}


