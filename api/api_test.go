package api_test



import(

	"testing"
	//"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/redis/api"
	//"time"
	"github.com/magiconair/properties/assert"
	"time"
	"fmt"
)






//func GetRedisPool( redisUrl string) redis.Pool {
//	pool :=  redis.Pool{
//		MaxIdle: 3,
//		//MaxActive: 50,
//		IdleTimeout: 240 * time.Second,
//		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },
//
//		TestOnBorrow: func(c redis.Conn, t time.Time) error {
//			_, err := c.Do("PING")
//			return err
//		},
//	}
//	return pool
//}


func TestExecRedisCommand(t *testing.T) {


	pool := api.GetRedisPool("redis://localhost:6379/0")
	cnx := pool.Get()
	defer cnx.Close()


	cnx.Do("FLUSHDB")

	args := []interface{}{"there"}
	reply,err := api.ExecRedisCommand(cnx,"SET" ,"hello", args )
	assert.Equal(t,err,nil)

	text,kind,err := api.Jsonify(reply)
	assert.Equal(t,err,nil)
	assert.Equal(t,kind,"string")
	assert.Equal(t,string(text),"\"OK\"")


	args2 := []interface{}{}
	reply2,err := api.ExecRedisCommand(cnx,"INCR" ,"counter", args2 )
	assert.Equal(t,err,nil)

	text2,kind2,err := api.Jsonify(reply2)
	assert.Equal(t,err,nil)
	assert.Equal(t,kind2,"integer")
	assert.Equal(t,string(text2),"1")


	args3 := []interface{}{}
	reply3,err := api.ExecRedisCommand(cnx,"keys" ,"*", args3 )
	assert.Equal(t,err,nil)

	text3,kind3,err := api.Jsonify(reply3)
	assert.Equal(t,err,nil)
	assert.Equal(t,kind3,"list")
	print(string(text3))
	assert.Equal(t,string(text3),"[\"counter\",\"hello\"]")



	_,_ = api.ExecRedisCommand(cnx,"HSET","dict", []interface{}{"k1","v1"})
	_,_ = api.ExecRedisCommand(cnx,"HSET","dict", []interface{}{"k2","v2"})

	reply4,err := api.ExecRedisCommand(cnx,"HGETALL","dict", nil)
	text4,kind4,err := api.Jsonify(reply4)
	assert.Equal(t,err,nil)
	assert.Equal(t,kind4,"list")
	print(string(text4))
	assert.Equal(t,string(text4),"[\"k1\",\"v1\",\"k2\",\"v2\"]")


	reply5,err := api.ExecRedisCommand(cnx,"EXISTS","hello", nil)
	text5,kind5,err := api.Jsonify(reply5)
	assert.Equal(t,err,nil)
	assert.Equal(t,kind5,"integer")
	print(string(text5))
	assert.Equal(t,string(text5),"1")


	reply6,err := api.ExecRedisCommand(cnx,"GET","hello", nil)
	text6,kind6,err := api.Jsonify(reply6)
	assert.Equal(t,err,nil)
	assert.Equal(t,kind6,"string")
	print(string(text6))
	assert.Equal(t,string(text6),"\"there\"")


}





func TestServer(t *testing.T) {

	srv := api.StartHttpServer()

	time.Sleep(10 * time.Second)

	fmt.Print("Shutdown server")
	srv.Shutdown(nil)


}




func TestRedisSet(t *testing.T) {


	pool := api.GetRedisPool("redis://localhost:6379/0")
	cnx := pool.Get()
	defer cnx.Close()

	key := "hello:key"
	value := "hello"


	// SETEX mykey 10 "Hello"


	_,err := cnx.Do("SETEX" , key , "10",  value)
	assert.Equal(t,err,nil)



	var args  api.RedisArgs
	args= append(args,10)
	args= append(args,"hello")

	//{"10","hello"}


	//_,err = cnx.Do("SETEX" , key , args[:]...)
	//assert.Equal(t,err,nil)

	_,err = api.ExecRedisCommand(cnx,"SETEX", "helllo:there",args)
	assert.Equal(t,err,nil)



}

