package api


import (

	//"flag"
	"net/http"
	"log"
	//"github.com/garyburd/redigo/redis"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/_listeners"
	"strings"
	"errors"
	"github.com/garyburd/redigo/redis"
	"io/ioutil"
	"fmt"
	"encoding/json"
	//"time"
)




type RedisArgs []interface{}




func SplitPath( path string ) ( command,key string, err error) {

	parts := strings.Split(path,"/")
	if len (parts) != 4 {
		err = errors.New("bad path format")
		return command,key,err
	}
	command = strings.ToUpper(parts[2])
	key= parts[3]
	return command,key,err
}



func ExecRedisCommand( cnx redis.Conn , command string,key string, args []interface{}) (reply interface{}, err error){

	switch len(args){
	case 0:
		return cnx.Do(command,key)
	case 1:
		return cnx.Do(command,key,args[0])
	case 2:
		return cnx.Do(command,key,args[0],args[1])
	case 3:
		return cnx.Do(command,key,args[0],args[1],args[2])
	default:
		return cnx.Do(command,key,args )
	}
}


func Jsonify( value interface{}) ( json_text []uint8, kind string, err error) {

	switch v := value.(type) {
	case int64:
		// v is an int here, so e.g. v + 1 is possible.
		//fmt.Printf("int64: %v\n", value)
		json_text ,err := json.Marshal(value)
		if err != nil {
			return json_text,"",err
		}
		return json_text,"integer",err

	case string:
		// v is an int here, so e.g. v + 1 is possible.
		//fmt.Printf("string: %v\n", value)
		json_text ,err := json.Marshal(value)
		if err != nil {
			return json_text,"",err
		}
		return json_text,"string",err

		//case bool:
		//	fmt.Printf("bool: %v\n", value)
		//	json_text ,err := json.Marshal(value)
		//	if err != nil {
		//		return json_text,"",err
		//	}
		//	return json_text,"bool",err


	case []byte:
		//fmt.Printf("bytes: %v\n", string(value.([]uint8)))
		json_text ,err := json.Marshal(string(value.([]uint8)))
		if err != nil {
			return json_text,"",err
		}
		return json_text,"string",err

	case []interface{}:
		//fmt.Printf("array of interface: %v\n", value)

		data := []string{}
		for _,e := range value.([]interface{}){

			switch e.(type){
				case []uint8:
					data = append(data,string(e.([]uint8)))
				//case []interface{}:
				//	data = append(data,string(e.([]interface{})))
			}
			//data = append(data,string(e.([]uint8)))
		}

		json_text ,err := json.Marshal(data)
		if err != nil {
			return json_text,"",err
		}
		return json_text,"list",err

	case nil:
		json_text ,err := json.Marshal(value)
		if err != nil {
			return json_text,"",err
		}
		return json_text,"nil",err

	default:
		fmt.Printf("cannot handle type  %s\n", v)
	}


	return json_text,"",err
}



//func PingHandler(w http.ResponseWriter, r *http.Request) {
//
//	HandleSimpleCommand(w,r,"PING")
//
//	//cnx := Pool.Get()
//	//defer cnx.Close()
//	//
//	//reply,err := cnx.Do("PING")
//	//if err != nil {
//	//	http.Error(w, err.Error() , 500)
//	//	return
//	//}
//	//text,_,err := Jsonify(reply)
//	//w.Header().Set("content-type", "application/json")
//	//w.Write(text)
//
//}

//func SaveHandler(w http.ResponseWriter, r *http.Request) {
//
//	cnx := Pool.Get()
//	defer cnx.Close()
//
//	reply,err := cnx.Do("SAVE")
//	if err != nil {
//		http.Error(w, err.Error() , 500)
//		return
//	}
//	text,_,err := Jsonify(reply)
//	w.Header().Set("content-type", "application/json")
//	w.Write(text)
//
//}


func HandleSimpleCommand( w http.ResponseWriter, r *http.Request,  cmd string, ) {

	// handle single word command like PING/SAVE

	cnx := Pool.Get()
	defer cnx.Close()

	reply,err := cnx.Do(cmd)
	if err != nil {
		http.Error(w, err.Error() , 500)
		return
	}
	text,_,err := Jsonify(reply)
	w.Header().Set("content-type", "application/json")
	w.Write(text)

}



func RawHandler(w http.ResponseWriter, r *http.Request) {

	// handle /raw/*

	path := r.URL.String()
	log.Printf("url: %s\n",path)
	command,key,err := SplitPath(path)
	if err != nil {
		w.Write([]byte(`error`))
		return
	}

	args := RedisArgs{}
	switch r.Method {

	case "GET":
		body  := r.URL.Query().Get("body")
		_=body
		http.Error(w, "Method Not Allowed" , 405)
		return

	case "POST":

		raw, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		if len(raw) > 0 {
			err = json.Unmarshal(raw, &args)
			if err != nil {
				http.Error(w, err.Error(), 400)
				return
			}
		}
	default:
		http.Error(w, "Method Not Allowed" , 405)
		return
	}

	cnx := Pool.Get()
	defer cnx.Close()

	reply,err := ExecRedisCommand(cnx,command,key,args)
	if err != nil {
		http.Error(w, err.Error() , 500)
		return
	}

	text,kind,err := Jsonify(reply)
	_=kind

	w.Header().Set("content-type", "application/json")
	w.Write(text)
	return

}






