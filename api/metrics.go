package api

import (

	"bitbucket.org/cocoon_bitbucket/redis/exporter"
	"github.com/prometheus/client_golang/prometheus"
	//"github.com/prometheus/client_golang/prometheus/promhttp"
	//log "github.com/sirupsen/logrus"
	"log"
	"strings"
)

var (

	addrs            []string
	passwords        []string
	aliases          []string

)



func InitMetrics() {


	addrs, passwords, aliases = loadRedisArgs(*redisAddr, *redisPassword, *redisAlias, *separator)


	exp, err := exporter.NewRedisExporter(
		exporter.RedisHost{Addrs: addrs, Passwords: passwords, Aliases: aliases},
		*namespace,
		*checkKeys)
	if err != nil {
		log.Fatal(err)
	}

	//buildInfo := prometheus.NewGaugeVec(prometheus.GaugeOpts{
	//	Name: "redis_exporter_build_info",
	//	Help: "redis exporter build_info",
	//}, []string{"version", "commit_sha", "build_date", "golang_version"})
	//buildInfo.WithLabelValues(VERSION, COMMIT_SHA1, BUILD_DATE, runtime.Version()).Set(1)

	prometheus.MustRegister(exp)
	//prometheus.MustRegister(buildInfo)

	//http.Handle(*metricPath, prometheus.Handler())


}


func loadRedisArgs(addr, password, alias, separator string) ([]string, []string, []string) {

	if addr == "" {
		addr = "redis://localhost:6379"
	}
	addrs = strings.Split(addr, separator)
	passwords = strings.Split(password, separator)
	for len(passwords) < len(addrs) {
		passwords = append(passwords, passwords[0])
	}
	aliases = strings.Split(alias, separator)
	for len(aliases) < len(addrs) {
		aliases = append(aliases, aliases[0])
	}
	return addrs, passwords, aliases
}