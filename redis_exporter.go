package main



import (

	"bitbucket.org/cocoon_bitbucket/redis/api"
	"fmt"
	//"time"
	"os"
	"os/signal"
	"syscall"
	"time"
)




func main(){

	// wait for redis to come up
	time.Sleep(2*time.Second)

	// api.Server()
	srv := api.StartHttpServer()
	_=srv

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		//syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	go func() {
		s := <-sigc
		// now close the server gracefully ("shutdown")
		// timeout could be given instead of nil as a https://golang.org/pkg/context/
		if err := srv.Shutdown(nil); err != nil {
			panic(err) // failure/timeout shutting down the server gracefully
		}
		fmt.Printf("main: exiting after receiving signal: [%s]\n",s)
		os.Exit(0)
	}()

	// run for 60 second
	//time.Sleep(60 * time.Second)

	// block forever
	select{ }


	fmt.Printf("main: exiting unexpecttidly\n")


}